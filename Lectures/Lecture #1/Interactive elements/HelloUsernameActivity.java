package com.example.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.view.*;
import android.util.Log;
public class HelloUsernameActivity extends Activity implements View.OnClickListener
{
    private EditText nameEditText;
    private TextView messageText;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_username);

        final View btnSubmit;

        nameEditText = findViewById(R.id.input_name);
        messageText = findViewById(R.id.message_text);
        btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.d("HelloWorld", "HelloUsernameActivity.onClick: clicked on view " + v);
        if (v.getId() == R.id.btn_submit) {
            onSubmitBtnClicked();
        }
    }

    private void onSubmitBtnClicked() {
        String name = nameEditText.getText().toString();
        String message = getString(R.string.hello_username, name);
        messageText.setText(message);
    }

}

